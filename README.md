[![build status](https://gitlab.com/ixilon/etag-header-spring-boot-autoconfigure/badges/master/build.svg)](https://gitlab.com/ixilon/etag-header-spring-boot-autoconfigure/commits/master)
[![Maven Central](https://maven-badges.herokuapp.com/maven-central/de.ixilon/etag-header-spring-boot-autoconfigure/badge.svg)](https://maven-badges.herokuapp.com/maven-central/de.ixilon/etag-header-spring-boot-autoconfigure)

# etag-header-spring-boot-autoconfigure #

Add a constant HTTP ETag response header defined by the property `http.header.etag`.
