package de.ixilon.starter.etag;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpMethod;
import org.springframework.web.filter.OncePerRequestFilter;

public class EtagHeaderFilter extends OncePerRequestFilter {

  private final String etag;

  public EtagHeaderFilter(String etag) {
    this.etag = etag;
  }

  @Override
  protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
      throws ServletException, IOException {
    String method = request.getMethod();
    if (HttpMethod.GET.matches(method) || HttpMethod.HEAD.matches(method)) {
      response.addHeader("ETag", etag);
      if (etag.equals(request.getHeader("If-None-Match"))) {
        response.setStatus(HttpServletResponse.SC_NOT_MODIFIED);
        return;
      }
    }
    filterChain.doFilter(request, response);
  }

}
