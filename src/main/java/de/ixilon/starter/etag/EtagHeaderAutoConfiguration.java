package de.ixilon.starter.etag;

import javax.servlet.Filter;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConditionalOnProperty("http.header.etag")
public class EtagHeaderAutoConfiguration {
  
  @Value("${http.header.etag}")
  private String etag;

  @Bean
  public Filter etagHeaderFilter() {
    return new EtagHeaderFilter(etag);
  }

}
